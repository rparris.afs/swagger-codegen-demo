package com.example.swaggercodegendemo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PetDto {
    @NotBlank
    private String name;

    @NotNull
    private PetType petType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PetType getPetType() {
        return petType;
    }

    public void setPetType(PetType petType) {
        this.petType = petType;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PetDto{");
        sb.append("name='").append(name).append('\'');
        sb.append(", petType=").append(petType);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PetDto)) return false;

        PetDto petDto = (PetDto) o;

        if (name != null ? !name.equals(petDto.name) : petDto.name != null) return false;
        return petType == petDto.petType;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (petType != null ? petType.hashCode() : 0);
        return result;
    }
}
