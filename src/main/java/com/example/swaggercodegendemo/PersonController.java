package com.example.swaggercodegendemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    @GetMapping
    public PersonDto getPerson() {
        return new PersonDto();
    }

    @PostMapping
    public void addPerson(@RequestBody @Valid PersonDto person) {
        System.out.println("valid person " + person.toString());
    }
}
