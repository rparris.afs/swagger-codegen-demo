package com.example.swaggercodegendemo;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class PersonDto {
    @NotNull
    private String id;

    @NotBlank
    private String name;

    @Min(0)
    @Max(100)
    private int age;

    private String nickName;

    @Size(min = 1, max = 3)
    private List<String> interests;

    private List<PetDto> pets;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public List<String> getInterests() {
        return interests;
    }

    public void setInterests(List<String> interests) {
        this.interests = interests;
    }

    public List<PetDto> getPets() {
        return pets;
    }

    public void setPets(List<PetDto> pets) {
        this.pets = pets;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PersonDto{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", age=").append(age);
        sb.append(", nickName='").append(nickName).append('\'');
        sb.append(", interests=").append(interests);
        sb.append(", pets=").append(pets);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonDto)) return false;

        PersonDto personDto = (PersonDto) o;

        if (age != personDto.age) return false;
        if (id != null ? !id.equals(personDto.id) : personDto.id != null) return false;
        if (name != null ? !name.equals(personDto.name) : personDto.name != null) return false;
        if (nickName != null ? !nickName.equals(personDto.nickName) : personDto.nickName != null) return false;
        if (interests != null ? !interests.equals(personDto.interests) : personDto.interests != null) return false;
        return pets != null ? pets.equals(personDto.pets) : personDto.pets == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + age;
        result = 31 * result + (nickName != null ? nickName.hashCode() : 0);
        result = 31 * result + (interests != null ? interests.hashCode() : 0);
        result = 31 * result + (pets != null ? pets.hashCode() : 0);
        return result;
    }
}
