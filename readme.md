~/projects/fap2/swagger-codegen-demo master ?1 ❯ openapi-generator generate -i http://localhost:8080/v3/api-docs -g javascript -o ./generated/javascript
~/projects/fap2/swagger-codegen-demo master ?1 ❯ openapi-generator generate -i http://localhost:8080/v3/api-docs -g javascript -o ./generated/javascript-with-options --additional-properties=useES6=true,usePromises=true
~/projects/fap2/swagger-codegen-demo master ?1 ❯ openapi-generator generate -i http://localhost:8080/v3/api-docs -g typescript-axios -o ./generated/typescript-axios
~/projects/fap2/swagger-codegen-demo master ?1 ❯ openapi-generator generate -i http://localhost:8080/v3/api-docs -g typescript-fetch -o ./generated/typescript-fetch
~/projects/fap2/swagger-codegen-demo master ?1 ❯ openapi-generator generate -i http://localhost:8080/v3/api-docs -g typescript-fetch -o ./generated/typescript-fetch-with-options --additional-properties=supportsES6=true,typescriptThreePlus=true
