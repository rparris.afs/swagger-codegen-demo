# OpenApiDefinition.PersonDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**name** | **String** |  | 
**age** | **Number** |  | [optional] 
**nickName** | **String** |  | [optional] 
**interests** | **[String]** |  | [optional] 
**pets** | [**[PetDto]**](PetDto.md) |  | [optional] 


