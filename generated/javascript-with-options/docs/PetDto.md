# OpenApiDefinition.PetDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**petType** | **String** |  | 



## Enum: PetTypeEnum


* `DOG` (value: `"DOG"`)

* `CAT` (value: `"CAT"`)

* `FISH` (value: `"FISH"`)

* `SOMETHING_STUPID` (value: `"SOMETHING_STUPID"`)




