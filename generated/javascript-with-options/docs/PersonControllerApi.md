# OpenApiDefinition.PersonControllerApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addPerson**](PersonControllerApi.md#addPerson) | **POST** /api/person | 
[**getPerson**](PersonControllerApi.md#getPerson) | **GET** /api/person | 



## addPerson

> addPerson(personDto)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.PersonControllerApi();
let personDto = new OpenApiDefinition.PersonDto(); // PersonDto | 
apiInstance.addPerson(personDto).then(() => {
  console.log('API called successfully.');
}, (error) => {
  console.error(error);
});

```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **personDto** | [**PersonDto**](PersonDto.md)|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined


## getPerson

> PersonDto getPerson()



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.PersonControllerApi();
apiInstance.getPerson().then((data) => {
  console.log('API called successfully. Returned data: ' + data);
}, (error) => {
  console.error(error);
});

```

### Parameters

This endpoint does not need any parameter.

### Return type

[**PersonDto**](PersonDto.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

