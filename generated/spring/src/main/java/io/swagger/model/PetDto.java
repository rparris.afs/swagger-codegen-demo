package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PetDto
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-05-01T12:16:40.594012-04:00[America/New_York]")
public class PetDto   {
  @JsonProperty("name")
  private String name = null;

  /**
   * Gets or Sets petType
   */
  public enum PetTypeEnum {
    DOG("DOG"),
    
    CAT("CAT"),
    
    FISH("FISH"),
    
    SOMETHING_STUPID("SOMETHING_STUPID");

    private String value;

    PetTypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static PetTypeEnum fromValue(String text) {
      for (PetTypeEnum b : PetTypeEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }
  @JsonProperty("petType")
  private PetTypeEnum petType = null;

  public PetDto name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(required = true, value = "")
      @NotNull

    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PetDto petType(PetTypeEnum petType) {
    this.petType = petType;
    return this;
  }

  /**
   * Get petType
   * @return petType
  **/
  @ApiModelProperty(required = true, value = "")
      @NotNull

    public PetTypeEnum getPetType() {
    return petType;
  }

  public void setPetType(PetTypeEnum petType) {
    this.petType = petType;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PetDto petDto = (PetDto) o;
    return Objects.equals(this.name, petDto.name) &&
        Objects.equals(this.petType, petDto.petType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, petType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PetDto {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    petType: ").append(toIndentedString(petType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
